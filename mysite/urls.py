from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',

    # The View related to the Home page
    
    url(r'^$', 'mysite.views.home', name='home'),
    
    # Views related to the URLs in the Polls app
    # namespace is required when using names in the polls app to direct the urls
    # example: if trying to direct to /polls/ instead of setting an href
    #          to "/polls/" or "{% url 'index' %}", you must use "{% url 'polls:index' %}"  
    
    url(r'^polls/', include('polls.urls', namespace='polls')),
    
    # Views related to the Admin interface
    
    (r'^grappelli/', include('grappelli.urls')), 
    url(r'^admin/', include(admin.site.urls)),
)
