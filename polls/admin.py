from django.contrib import admin
from .models import Poll, Choice

# Create an inline model for the Choice model
# There is a StackInline and TabularInline, the latter uses less screen real estate
class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 0 # Creates extra choices in the admin interface that are initialized empty


# Custom adminModel definitions
class PollAdmin(admin.ModelAdmin):
    """ Define what appears when selecting a Poll object in the admin interface
           fields - what order the fields occur in from top to bottom
           inlines - what models can be displayed inline instead of having their own interface
           list_display - what is displayed when display all Poll objects
           list_filter - adds an extra field to filter by, for when dataset get large. Is dependant
                         on th type of field being filtered on. In this case it's a DateTimeField
           search_fields - adds a search bar, allows searching by that field
    """
    
    fields = ['question', 'pub_date'] 
    inlines = [ChoiceInline] 
    list_display = ['pub_date', 'question', 'was_published_recently']
    list_filter = ['pub_date']
    search_fields = ['question']

# Register the Models within the Django admin interface
admin.site.register(Poll, PollAdmin)

