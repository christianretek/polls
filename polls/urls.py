from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^$', 'polls.views.index', name='index'),
    url(r'^(?P<poll_id>\d+)$', 'polls.views.details', name='details'),
    url(r'^(?P<poll_id>\d+)/results/$', 'polls.views.results', name='results'),
    url(r'^(?P<poll_id>\d+)/vote/$', 'polls.views.vote', name='vote'),
)
