from django.shortcuts import render_to_response, get_object_or_404, HttpResponseRedirect
from django.template import RequestContext
from django.core.urlresolvers import reverse

from .models import Poll, Choice

def index(request):
    context = RequestContext(request)
    context_dict = {}
    
    # Get the top 5 most recent poll objects
    latest_polls_list = Poll.objects.order_by('-pub_date')[:5]
    context_dict['latest_polls_list'] = latest_polls_list
    return render_to_response('polls/index.html', context_dict, context)

def details(request, poll_id):
    context = RequestContext(request)
    context_dict = {}
    
    poll = get_object_or_404(Poll, pk=poll_id)
    context_dict['poll'] = poll
    
    return render_to_response('polls/details.html', context_dict, context)

def results(request, poll_id):
    context = RequestContext(request)
    context_dict = {}    
    
    poll = get_object_or_404(Poll, pk=poll_id)
    context_dict['poll'] = poll
    return render_to_response('polls/results.html', context_dict, context)

def vote(request, poll_id):
    # Get the context from the request, init an empty dict to be passed
    context = RequestContext(request)
    context_dict = {}
    
    p = get_object_or_404(Poll, pk=poll_id)
    context_dict['poll'] = p
    
    try:
        selected_choice = p.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        context_dict['error_message'] = "You didn't select a choice."
        # Redisplay the poll voting form.
        return render_to_response('polls/details.html', context_dict, context)
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('polls:results', args=(p.id,)))


        
